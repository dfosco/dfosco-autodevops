## Autodevoops Let's Go!!!

This website is powered by AWS with GitLab Autodevops and [Hugo](https://gohugo.io), and can be built in under 1 minute.

Literally. It uses the `beautifulhugo` theme which supports content on your front page.
Edit `/content/_index.md` to change what appears here. Delete `/content/_index.md`
if you don't want any content here.

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.
